﻿namespace TurtlessForTesting.Console.Interfaces
{
    public interface ILogger
    {
        void LogInfo(string message);
    }
}