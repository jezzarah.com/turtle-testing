﻿using System;
using TurtlessForTesting.Console.Interfaces;
using TurtlessForTesting.Console.Models;

namespace TurtlessForTesting.Console
{
    public class TurtleCommandInterpreter
    {
        private readonly Turtle _turtle;
        private readonly ILogger _logger;

        public TurtleCommandInterpreter(Turtle turtle, ILogger logger)
        {
            _turtle = turtle ?? throw new ArgumentNullException(nameof(turtle));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void ExecuteCommand(string command)
        {
            if (string.IsNullOrEmpty(command))
                return;

            string[] parts = command.Split(' ');
            string operation = parts[0];

            switch (operation.ToUpper())
            {
                case "PLACE":
                    if (parts.Length != 2)
                    {
                        _logger.LogInfo("'PLACE' can't be empty");
                        break;
                    }
                    var input = new InputData(parts[1].Split(','));
                    if (!input.TryParse())
                    {
                        _logger.LogInfo("Invalid 'PLACE' format.");
                        break;
                    }

                    _turtle.Place(input.X, input.Y, input.Direction);
                    break;

                case "MOVE":
                    _turtle.Move();
                    break;

                case "LEFT":
                    _turtle.RotateLeft();
                    break;

                case "RIGHT":
                    _turtle.RotateRight();
                    break;

                case "REPORT":
                    _logger.LogInfo("--- Output ---");
                    _logger.LogInfo(_turtle.Report());
                    break;

                default:
                    _logger.LogInfo("Invalid command.");
                    break;
            }
        }
    }
}