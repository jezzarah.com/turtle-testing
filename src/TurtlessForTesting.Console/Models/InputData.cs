using System;
using TurtlessForTesting.Console.Enums;

namespace TurtlessForTesting.Console.Models
{
    public class InputData
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public Direction Direction { get; private set; }

        private readonly string[] _inputParams;
        
        public InputData(string[] parameters)
        {
            _inputParams =  parameters ?? throw new ArgumentNullException(nameof(parameters));
        }

        public bool TryParse()
        {
            try
            {
                X = GetX();
                Y = GetY();
                Direction = GetDirection();
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private int GetX()
        {
            return int.Parse(_inputParams[0]);
        }
        
        private int GetY()
        {
            return int.Parse(_inputParams[1]);
        }

        private Direction GetDirection()
        {
            return (Direction)Enum.Parse(typeof(Direction), _inputParams[2].ToUpper());
        }
    }
}