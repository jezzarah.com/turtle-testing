﻿using TurtlessForTesting.Console.Interfaces;

namespace TurtlessForTesting.Console.Models
{
    public class ConsoleLogger : ILogger
    {
        public void LogInfo(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}