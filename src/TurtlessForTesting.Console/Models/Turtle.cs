﻿using System;
using TurtlessForTesting.Console.Enums;

namespace TurtlessForTesting.Console.Models
{
    public class Turtle
    {
        private const int MIN_X_POSITION = 0;
        private const int MIN_Y_POSITION = 0;

        private const int MAX_X_POSITION = 5;
        private const int MAX_Y_POSITION = 5;

        private int _x;
        private int _y;
        private Direction _direction;
        private bool _isPlaced;

        public Turtle()
        {
            _isPlaced = false;
        }

        public void Place(int x, int y, Direction direction)
        {
            if (!IsValidPosition(x, y)) return;
            _x = x;
            _y = y;
            _direction = direction;
            _isPlaced = true;
        }

        public void Move()
        {
            if (!_isPlaced)
                return;

            int newX = _x;
            int newY = _y;

            switch (_direction)
            {
                case Direction.NORTH:
                    newY++;
                    break;
                case Direction.SOUTH:
                    newY--;
                    break;
                case Direction.EAST:
                    newX++;
                    break;
                case Direction.WEST:
                    newX--;
                    break;
            }

            if (!IsValidPosition(newX, newY)) return;
            _x = newX;
            _y = newY;
        }

        public void RotateLeft()
        {
            if (!_isPlaced)
                return;

            _direction = GetNewDirection(_direction, Rotation.Left);
        }

        public void RotateRight()
        {
            if (!_isPlaced)
                return;

            _direction = GetNewDirection(_direction, Rotation.Right);
        }

        public string Report()
        {
            return !_isPlaced ? string.Empty : $"{_x},{_y},{_direction}";
        }

        private bool IsValidPosition(int x, int y)
        {
            return x >= MIN_X_POSITION &&
                   x < MAX_X_POSITION &&
                   y >= MIN_Y_POSITION &&
                   y < MAX_Y_POSITION;
        }
        
        private Direction GetNewDirection(Direction currentDirection, Rotation rotation)
        {
            int numDirections = Enum.GetValues(typeof(Direction)).Length;
            int currentDirectionValue = (int)currentDirection;
            
            // Calculate the new direction value by adding the rotation value and applying modulus
            // This ensures the resulting value wraps around within the valid direction range
            int newDirectionValue = (currentDirectionValue + (int)rotation + numDirections) % numDirections;
            return (Direction)newDirectionValue;
        }
    }
}