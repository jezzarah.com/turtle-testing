﻿namespace TurtlessForTesting.Console.Enums
{
    public enum Rotation
    {
        Left = -1, // Represents left rotation with a negative value
        Right = 1  // Represents right rotation with a positive value
    }
}