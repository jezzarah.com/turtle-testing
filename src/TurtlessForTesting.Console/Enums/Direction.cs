﻿namespace TurtlessForTesting.Console.Enums
{
    public enum Direction
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}