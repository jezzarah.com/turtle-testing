﻿using System.IO;
using TurtlessForTesting.Console.Interfaces;
using TurtlessForTesting.Console.Models;

namespace TurtlessForTesting.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger logger = new ConsoleLogger();
            logger.LogInfo("--- Input ---");
            Turtle turtle = new Turtle();
            TurtleCommandInterpreter interpreter = new TurtleCommandInterpreter(turtle, logger);

            while (true)
            {
                logger.LogInfo(
                    "Enter 'C' to enter commands from the console, or 'F' to specify a file path, or 'X' for exit:");
                string inputMode = System.Console.ReadLine()?.ToUpper();
                switch (inputMode)
                {
                    case "C":
                        logger.LogInfo("Enter commands (one command per line, press Enter to finish):");
                        while (true)
                        {
                            string command = System.Console.ReadLine();
                            if (string.IsNullOrEmpty(command))
                                break;

                            interpreter.ExecuteCommand(command);
                        }

                        break;
                    case "F":
                        logger.LogInfo("Enter the file path:");
                        string filePath = System.Console.ReadLine();

                        if (!string.IsNullOrEmpty(filePath))
                        {
                            try
                            {
                                string[] commands = File.ReadAllLines(filePath);
                                foreach (string command in commands)
                                {
                                    interpreter.ExecuteCommand(command);
                                }
                            }
                            catch (IOException ex)
                            {
                                logger.LogInfo($"Error reading file: {ex.Message}");
                            }
                        }
                        break;
                    case "X":
                        logger.LogInfo("Finished");
                        return;
                    default:
                        logger.LogInfo("Invalid Input command");
                        break;
                }
            }
        }
    }
}