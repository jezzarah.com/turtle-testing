using NUnit.Framework;
using TurtlessForTesting.Console.Enums;
using TurtlessForTesting.Console.Models;

namespace Tests
{
  [TestFixture]
    public class TurtleTests
    {
        [Test]
        public void Turtle_Move_ValidMove_Success()
        {
            Turtle turtle = new Turtle();
            turtle.Place(1, 1, Direction.SOUTH);
            
            turtle.Move();
            
            Assert.AreEqual("1,0,SOUTH", turtle.Report());
        }

        [Test]
        public void Turtle_Move_InvalidMove_NoChange()
        {
            Turtle turtle = new Turtle();
            turtle.Place(0, 4, Direction.NORTH);
            
            turtle.Move();

            Assert.AreEqual("0,4,NORTH", turtle.Report());
        }

        [Test]
        public void Turtle_RotateLeft_ValidRotation_Success()
        {
            Turtle turtle = new Turtle();
            turtle.Place(1, 1, Direction.NORTH);
            
            turtle.RotateLeft();
            
            Assert.AreEqual("1,1,WEST", turtle.Report());
        }

        [Test]
        public void Turtle_RotateRight_ValidRotation_Success()
        {
            Turtle turtle = new Turtle();
            turtle.Place(3, 3, Direction.WEST);
            
            turtle.RotateRight();
            
            Assert.AreEqual("3,3,NORTH", turtle.Report());
        }

        [Test]
        public void Turtle_Report_NotPlaced_ReturnsEmptyString()
        {
            Turtle turtle = new Turtle();
            
            string report = turtle.Report();
            
            Assert.AreEqual(string.Empty, report);
        }

        [Test]
        public void Turtle_Report_Placed_ReturnsCurrentPosition()
        {
            Turtle turtle = new Turtle();
            turtle.Place(0, 0, Direction.NORTH);
            
            string report = turtle.Report();
            
            Assert.AreEqual("0,0,NORTH", report);
        }

        [Test]
        public void Turtle_CommandSequence_ValidSequence_Success()
        {
            Turtle turtle = new Turtle();
            
            turtle.Place(1, 2, Direction.EAST);
            turtle.Move();
            turtle.Move();
            turtle.RotateLeft();
            turtle.Move();
            
            Assert.AreEqual("3,3,NORTH", turtle.Report());
        }
    }
}