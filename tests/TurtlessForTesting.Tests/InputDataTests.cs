﻿using NUnit.Framework;
using TurtlessForTesting.Console.Enums;
using TurtlessForTesting.Console.Models;

namespace Tests
{
    [TestFixture]
    public class InputDataTests
    {
        [Test]
        public void TryParse_ValidInput_ReturnsTrueAndSetsProperties()
        {
            string[] parameters = { "1", "2", "SOUTH" };
            InputData inputData = new InputData(parameters);
            
            bool result = inputData.TryParse();
            
            Assert.IsTrue(result);
            Assert.AreEqual(1, inputData.X);
            Assert.AreEqual(2, inputData.Y);
            Assert.AreEqual(Direction.SOUTH, inputData.Direction);
        }

        [Test]
        public void TryParse_InvalidInput_ReturnsFalseAndDisplaysErrorMessage()
        {
            string[] parameters = { "1", "2", "INVALID_DIRECTION" };
            InputData inputData = new InputData(parameters);
            
            bool result = inputData.TryParse();
            
            Assert.IsFalse(result);
        }
    }
}